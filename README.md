# AutoSD Buildbox Cross Compilation Tooling for x86_64-to-aarch64 

This container image provides a development environemnt for those who want to
perform cross compilation tasks for their AutoSD build stack.

## License

[MIT](./LICENSE)
